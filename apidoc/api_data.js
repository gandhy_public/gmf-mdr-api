define({ "api": [
  {
    "type": "post",
    "url": "/api/v1/auth/login",
    "title": "1. Login dan getToken",
    "version": "0.1.0",
    "name": "login",
    "group": "Authentication",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk cek username dan password, dan jika berhasil maka akan melakukan generate Token.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Required Username dari user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Required Password dari user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n\t\"username\": \"rano.dts\"\n\t\"password\": \"libranofauzan\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.nama",
            "description": "<p>Nama user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.email",
            "description": "<p>Email user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.jabatan",
            "description": "<p>Jabatan user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.account_name",
            "description": "<p>Account name user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.account_type",
            "description": "<p>Account type user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.username",
            "description": "<p>Username user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.token",
            "description": "<p>Token key user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t\"nama\": \"Librano Fauzan\",\n\t\t\"email\": \"rano.dts@gmf-aeroasia.co.id\",\n\t\t\"jabatan\": \"3rd Party - Programmer\",\n\t\t\"account_name\": \"rano.dts\",\n\t\t\"account_type\": \"805306368\",\n\t\t\"username\": \"rano.dts\",\n\t\t\"token\": \"c3130467372ddb6a190bb873c3a73263\"\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Akun LDAP tidak ada!\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controller/Auth.php",
    "groupTitle": "Authentication",
    "sampleRequest": [
      {
        "url": "http://localhost/gmf/app_mdr/public/index.php/api/v1/auth/login"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/v1/mdr/get-image/:nama_file",
    "title": "3. Get View Image",
    "version": "0.1.0",
    "name": "get_Image",
    "group": "Function_SAP",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk menampilkan gambar yang sudah diupload sebelumnya</p>",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n\t\"nama_file\": image.jpg\n}",
          "type": "string"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controller/Mdr.php",
    "groupTitle": "Function_SAP"
  },
  {
    "type": "post",
    "url": "/api/v1/mdr/get-order",
    "title": "1. Get Order",
    "version": "0.1.0",
    "name": "get_Order",
    "group": "Function_SAP",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengambil data Order sesuai dengan nomor order yang dikirimkan</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "order",
            "description": "<p>Required Order number parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"order\": your order number\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.kode_order",
            "description": "<p>kode order untuk melihat ada atau tidaknya data order di SAP.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"kode_order\": \"order valid\",\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controller/Mdr.php",
    "groupTitle": "Function_SAP",
    "sampleRequest": [
      {
        "url": "http://localhost/gmf/app_mdr/public/index.php/api/v1/mdr/get-order"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/v1/mdr/upload-image",
    "title": "2. Upload Image",
    "version": "0.1.0",
    "name": "upload_Image",
    "group": "Function_SAP",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengupload data Image</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "Files[]",
            "optional": false,
            "field": "image[]",
            "description": "<p>Required Data image file.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"image\": your image file\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.kode",
            "description": "<p>kode untuk melihat sukses atau tidaknya saat melakukan upload image.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"kode\": \"upload image sukses\",\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controller/Mdr.php",
    "groupTitle": "Function_SAP"
  },
  {
    "type": "post",
    "url": "/api/v1/master/get-cause_code",
    "title": "1. Get Cause Code",
    "version": "0.1.0",
    "name": "get_CauseCode",
    "group": "Master",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengambil data Cause Code dari database</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "select[]",
            "defaultValue": "*",
            "description": "<p>Optional select parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "where[]",
            "defaultValue": "null",
            "description": "<p>Optional where condition parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"select\": column name, column name\n\t\"where\": column name = foo, column name like %foo%\n}\nin Script PHP\n<php\n\t$select = array(\"column name\",\"column name\");\n\t$where = array(\"column name='foo'\",\"column name like %foo%\");\n?>",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.column_name",
            "description": "<p>name Data dari kolom yang dipilih.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"id_cause\": \"ZPM-STR_AC\",\n\t\t\t\"group_code\": \"ZPM-STR\",\n\t\t\t\"subcause\": \"AC\",\n\t\t\t\"desc_causecode\": \"Accidental damage\"\n\t\t},\n\t\t{\n\t\t\t\"id_cause\": \"ZPM-STR_BL\",\n\t\t\t\"group_code\": \"ZPM-STR\",\n\t\t\t\"subcause\": \"BL\",\n\t\t\t\"desc_causecode\": \"Blockage\"\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controller/Master.php",
    "groupTitle": "Master",
    "sampleRequest": [
      {
        "url": "http://localhost/gmf/app_mdr/public/index.php/api/v1/master/get-cause_code"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/v1/master/get-mdr_code",
    "title": "2. Get MDR Code",
    "version": "0.1.0",
    "name": "get_MdrCode",
    "group": "Master",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengambil data MDR Code dari database</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "select[]",
            "defaultValue": "*",
            "description": "<p>Optional select parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "where[]",
            "defaultValue": "null",
            "description": "<p>Optional where condition parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"select\": column name, column name\n\t\"where\": column name = foo, column name like %foo%\n}\nin Script PHP\n<php\n\t$select = array(\"column name\",\"column name\");\n\t$where = array(\"column name='foo'\",\"column name like %foo%\");\n?>",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.column_name",
            "description": "<p>name Data dari kolom yang dipilih.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"code\": \"ZPM-AVI\",\n\t\t\t\"desc\": \"Avionic Defect Codes\"\n\t\t},\n\t\t{\n\t\t\t\"code\": \"ZPM-CMC\",\n\t\t\t\"desc\": \"Complex Maintenance Codes\"\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controller/Master.php",
    "groupTitle": "Master",
    "sampleRequest": [
      {
        "url": "http://localhost/gmf/app_mdr/public/index.php/api/v1/master/get-mdr_code"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/v1/master/get-station_type",
    "title": "3. Get Station Type",
    "version": "0.1.0",
    "name": "get_StationType",
    "group": "Master",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengambil data Station Type dari database</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "select[]",
            "defaultValue": "*",
            "description": "<p>Optional select parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "where[]",
            "defaultValue": "null",
            "description": "<p>Optional where condition parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"select\": column name, column name\n\t\"where\": column name = foo, column name like %foo%\n}\nin Script PHP\n<php\n\t$select = array(\"column name\",\"column name\");\n\t$where = array(\"column name='foo'\",\"column name like %foo%\");\n?>",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.column_name",
            "description": "<p>name Data dari kolom yang dipilih.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"tipe_station\": \"AE\",\n\t\t\t\"desc\": \"Aileron\"\n\t\t},\n\t\t{\n\t\t\t\"code\": \"BBL\",\n\t\t\t\"desc\": \"Body Buttock Line\"\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controller/Master.php",
    "groupTitle": "Master",
    "sampleRequest": [
      {
        "url": "http://localhost/gmf/app_mdr/public/index.php/api/v1/master/get-station_type"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/v1/master/get-sub_code",
    "title": "4. Get Sub Code",
    "version": "0.1.0",
    "name": "get_SubCode",
    "group": "Master",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengambil data Sub Code dari database</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "select[]",
            "defaultValue": "*",
            "description": "<p>Optional select parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "where[]",
            "defaultValue": "null",
            "description": "<p>Optional where condition parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"select\": column name, column name\n\t\"where\": column name = foo, column name like %foo%\n}\nin Script PHP\n<php\n\t$select = array(\"column name\",\"column name\");\n\t$where = array(\"column name='foo'\",\"column name like %foo%\");\n?>",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.column_name",
            "description": "<p>name Data dari kolom yang dipilih.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"id_subcode\": \"ZPM-AVI_AC\",\n\t\t\t\"group_code\": \"ZPM-AVI\",\n\t\t\t\"subcode\": \"AC\",\n\t\t\t\"desc_subcode\" \"Arcing-connector\"\n\t\t},\n\t\t{\n\t\t\t\"id_subcode\": \"ZPM-AVI_AS\",\n\t\t\t\"group_code\": \"ZPM-AVI\",\n\t\t\t\"subcode\": \"AS\",\n\t\t\t\"desc_subcode\" \"Arcing-splice\"\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controller/Master.php",
    "groupTitle": "Master",
    "sampleRequest": [
      {
        "url": "http://localhost/gmf/app_mdr/public/index.php/api/v1/master/get-sub_code"
      }
    ]
  }
] });
