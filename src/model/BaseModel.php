<?php
namespace GandhyApp\Model;
require __DIR__ . '/../../vendor/autoload.php';

use Pixie\Connection;

class BaseModel
{
    public function connect(){
        $config = array(
            'driver'    => 'sqlsrv', // Db driver
            'host'      => '192.168.240.107',
            'database'  => 'db_crmapps',
            'username'  => 'dev-crm',
            'password'  => 'p@ssw0rd',
            'charset'   => 'utf8', // Optional
            'collation' => 'utf8_unicode_ci', // Optional
            'prefix'    => ''
        );
        $con = new \Pixie\Connection('mysql', $config);        
        return $con->getQueryBuilder();
    }

    public function connect2(){
        $settings = [
            'host' => '192.168.240.107',
            'user' => 'dev-crm',
            'pass' => 'p@ssw0rd',
            'dbname' => 'db_crmapps',
            'driver' => 'sqlsrv'
        ];
        $pdo = new PDO("sqlsrv:server=" . $settings['host'] . ";Database=" . $settings['dbname'],$settings['user'], $settings['pass']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }

    public function connect3()
    {
        $serverName = "192.168.240.107"; //serverName\instanceName, portNumber (default is 1433)
        $connectionInfo = array( "Database"=>"db_crmapps", "UID"=>"dev-crm", "PWD"=>"p@ssw0rd");
        $conn = sqlsrv_connect( $serverName, $connectionInfo);
        return $conn;
    }
}
// Create a connection, once only.

?>