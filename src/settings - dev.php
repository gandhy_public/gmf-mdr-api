<?php
return [
    'settings' => [
        'displayErrorDetails' => false, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Database Setting Settings
        'db' => [
			/* db MDR DEV*/
            'host' => '192.168.240.107',
            'user' => 'usr_dss',
            'pass' => 'p@ssw0rd',
            'dbname' => 'db_MDRmobile',
            'driver' => 'sqlsrv'

			/* db MDR PROD*/
            /*'host' => '192.168.240.101',
            'user' => 'usr-mdr',
            'pass' => 'p@ssw0rd',
            'dbname' => 'db_MDRmobile',
            'driver' => 'sqlsrv'*/
        ],
		'db_crm' => [
			/* db MDR DEV*/
            'host' => '192.168.240.107',
            'user' => 'dev-crm',
            'pass' => 'p@ssw0rd',
            'dbname' => 'db_crmapps',
            'driver' => 'sqlsrv',			
			'schema' => 'dbo'
			
			/* db MDR PROD*/
			/*'host' => '192.168.240.101',
            'user' => 'usr-mdr',
            'pass' => 'p@ssw0rd',
            'dbname' => 'db_crm_info',
            'driver' => 'sqlsrv',			
			'schema' => 'dbo'*/
        ],
		'user_wsdl' => [     
			/* USER PI */
           /*  'user' => 'G533207',
            'pass' => 'Ecco2018'  */    
			
			'user' => 'N327104',
            'pass' => 'Welcome01' 
			
			/*'user' => 'N347113',
            'pass' => 'Welcome45'  */    					
        ],
		'uri_img' => [    
			'destination' => 'http://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/mdr/get-image/'
        ],
    ],
];
