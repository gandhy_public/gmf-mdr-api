<?php
namespace MdrApp\Controller;
use MdrApp\Model\M_mdr;
date_default_timezone_set("Asia/Jakarta");
ini_set('upload_max_filesize', '30M');
ini_set('post_max_size', '30M');

class Mdr
{
	protected $con;
	protected $M_mdr;
	protected $soap;
	protected $base_url;
	protected $path_image;
	protected $path_image_all;
	protected $user_wsdl;
	protected $pass_wsdl;
	protected $urlprefix_image;
	protected $conn_ftp;
	protected $host_ftp;
	protected $user_ftp;
	protected $pass_ftp;
	protected $folder_ftp;
	

	public function koneksi($conn,$soapp, $wsdl, $url_image, $ftp)
	{
		$this->con = $conn;
		$this->M_mdr = new M_mdr($conn);
		
		$this->user_wsdl = $wsdl['user'];
		$this->pass_wsdl = $wsdl['pass'];
		
		$this->urlprefix_image = $url_image['destination'];
		
		 $this->soap = $soapp;
		$url = sprintf("%s://%s%s",
			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME'],
			$_SERVER['REQUEST_URI']
		);
		$url = explode("public/index.php", $url);
		$this->base_url = $url[0];

		$where = [
			'is_active' => 1,
			'type' => 'image'
		];
		$get_config = $this->M_mdr->select([],$where,'TBL_MDR_MOBILE_CONFIG');
		$this->path_image = $get_config['resultdata'][0]['config'];

		$where_distinct = [
			'type' => 'image'
		];
		$get_config_all = $this->M_mdr->select_distinct(['config'],$where_distinct,'TBL_MDR_MOBILE_CONFIG');
		$this->path_image_all = $get_config_all['resultdata'];

		$this->host_ftp = $ftp['host'];
		$this->user_ftp = $ftp['user'];
		$this->pass_ftp = $ftp['pass'];
		$this->folder_ftp = $ftp['folder'];

		$this->conn_ftp = ftp_connect($this->host_ftp) or die("Couldn't connect to ".$ftp['host']);
		$ftpLogin = ftp_login($this->conn_ftp, $this->user_ftp, $this->pass_ftp);
		// turn passive mode on
		ftp_pasv($this->conn_ftp, true);
	}

	/**
	* @api {post} /api/v1/mdr/get-order 1. Get Order
	* @apiVersion 0.1.0
	* @apiName get_Order
	* @apiGroup Function SAP
	* @apiPermission public
	* @apiDescription digunakan untuk mengambil data Order sesuai dengan nomor order yang dikirimkan
	*
	*
	* @apiParam {String} token  Required Token for use this API.
	* @apiParam {String} order  Required Order number parameter.
	*
	* @apiParamExample {json} Request-Example:
	* in Try Out
	* {
	*	"token": your token
	*	"order": your order number
	* }
	*
	*
	* @apiSuccess {String} codestatus Response Status.
	* @apiSuccess {String} message Response Message.
	* @apiSuccess {Array[]} resultdata Response Data.
	* @apiSuccess {String} resultdata.kode_order kode order untuk melihat ada atau tidaknya data order di SAP.
	*
	* @apiSuccessExample {json} Success-Response:
	*{
	*	"codestatus": "S",
    *	"message": "Sukses",
    *	"resultdata": {
    *		{
    *			"kode_order": "order valid",
    *		}
    *	}
	*}
	*
	*
	* @apiErrorExample {json} Error-Response:
	* {
	*	"codestatus": "E",
    *	"message": "Error Message",
    *	"resultdata": [],
	* }
	*/
	public function get_Order()
	{
		try {
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_mdr->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$no_order = $_POST['order'];
			if(!isset($no_order)) throw new \InvalidArgumentException("Nomor order tidak boleh kosong", 1);
			if(empty($no_order)) throw new \InvalidArgumentException("Nomor order tidak boleh kosong",1);
			if(strlen($no_order) < 12){
				$no_order = sprintf('%012s',$no_order);
			}elseif(strlen($no_order) == 12){
				$no_order = $no_order;
			}else{
				throw new \InvalidArgumentException("Format nomor order salah!",1);
			}

			
			$SOAP_AUTH = array( 
				'login'    => $this->user_wsdl,
                'password' => $this->pass_wsdl,				
            );
			
			//$WSDL = $this->base_url."assets/qa_cekorder_sap.wsdl";
			//$WSDL = $this->base_url."assets/QA_SI_MDR_ORDCEKService.wsdl";
			$WSDL = $this->base_url."assets/PROD_SI_MDR_ORDCEK.wsdl";
			


			$this->soap->SoapClient($WSDL,$SOAP_AUTH);
			$params = array(
		    	'V_AUFNR' => "$no_order"
		    );
		    $result = $this->soap->SI_MDR_ORDCEK($params);
			//echo "<pre>";print_r($result); echo "</pre>";die();
			
			if($result->CODE_STATUS == "S"){
		    	//$kode_order = "order valid";
		    	return [
					'codestatus'	=> 'S',
					'message'		=> $result->MESSAGE,
					'resultdata'	=> [
						'BSC_STRDATE' => $result->BSC_STRDATE,
						'FLOC' => $result->FLOC,
						'MAT' => $result->MAT,
						'M_WORK_CENTER' => $result->M_WORK_CENTER,
						'ORDER_TITLE' => $result->ORDER_TITLE,
						'PM_PS' => $result->PM_PS,
						'REVISION' => $result->REVISION,
						'REV_TXT' => $result->REV_TXT,						
						'WORK_CENTER' => $result->WORK_CENTER 
					]
				];			
		    }else{
				if($result->MESSAGE == ""){
					throw new \InvalidArgumentException("Order not valid!", 1);						
				}else{
					return ['codestatus'	=> 'E',
						'message'		=> $result->MESSAGE,
						'resultdata'	=> []
					];
				}		    	    	
		    }		    
		} catch (\InvalidArgumentException $e) {			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	/**
	* @api {post} /api/v1/mdr/upload-image 2. Upload Image
	* @apiSampleRequest off
	* @apiVersion 0.1.0
	* @apiName upload_Image
	* @apiGroup Function SAP
	* @apiPermission public
	* @apiDescription digunakan untuk mengupload data Image
	*
	*
	* @apiParam {String} token  Required Token for use this API.
	* @apiParam {Files} image  Required Data image file.
	* @apiParam {String} transaction_id  Required ID Transaction.
	*
	* @apiParamExample {json} Request-Example:
	* in Try Out
	* {
	*	"token": your token
	*	"image": your image file
	*	"transaction_id" : your id transaction
	* }
	*
	*
	* @apiSuccess {String} codestatus Response Status.
	* @apiSuccess {String} message Response Message.
	* @apiSuccess {Array[]} resultdata Response Data.
	* @apiSuccess {String} resultdata.kode kode untuk melihat sukses atau tidaknya saat melakukan upload image.
	*
	* @apiSuccessExample {json} Success-Response:
	*{
	*	"codestatus": "S",
    *	"message": "Sukses",
    *	"resultdata": {
    *		{
    *			"kode": "upload image sukses",
    *		}
    *	}
	*}
	*
	*
	* @apiErrorExample {json} Error-Response:
	* {
	*	"codestatus": "E",
    *	"message": "Error Message",
    *	"resultdata": [],
	* }
	*/
	public function upload_Image()
	{
		try {
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1); 
			
			$cek_token = $this->M_mdr->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$id_trans = $_POST['transaction_id'];
			if(!isset($id_trans)) throw new \InvalidArgumentException("transaction_id tidak boleh kosong!", 1);
			if(empty($id_trans)) throw new \InvalidArgumentException("transaction_id tidak boleh kosong!", 1);

			$nama_gambar = $_POST['nama'];
			if(!isset($nama_gambar)) throw new \InvalidArgumentException("nama gambar tidak boleh kosong!", 1);
			if(empty($nama_gambar)) throw new \InvalidArgumentException("nama gambar tidak boleh kosong!", 1);

			$mdrno = $_POST['mdr_order'];
			if(!isset($mdrno)) throw new \InvalidArgumentException("Nomor MDR Order tidak boleh kosong!", 1);
			if(empty($mdrno)) throw new \InvalidArgumentException("Nomor MDR Order tidak boleh kosong!", 1);
 
			$master_gambar = $_FILES['image'];
			if(!isset($master_gambar)) throw new \InvalidArgumentException("Format image salah!", 1);
			if(empty($master_gambar)) throw new \InvalidArgumentException("Format image salah!", 1);
				
				$format_gambar = explode(".", $nama_gambar);
				$format_gambar = $format_gambar[1];
				$tmp_gambar = $master_gambar['tmp_name'];
				//$dst_gambar = "MDR_image/".$nama_gambar;
				$dst_gambar = $this->folder_ftp ."/". $nama_gambar;
				
				$url_gambar = $this->urlprefix_image . $nama_gambar;

				if(ftp_put($this->conn_ftp, $dst_gambar, $tmp_gambar, FTP_BINARY)){
				    $param = [
						'name' => $url_gambar,
						'create_at' => date('Y-m-d H:i:s'),
						'is_delete' => 0,
						'transaction_id' => $id_trans,
						'mdr_order' => $mdrno,
					];
					 $insert = $this->M_mdr->insert($param,'TBL_MDR_MOBILE_IMAGE');
					if($insert == FALSE) throw new \InvalidArgumentException("Insert image gagal!",0); 
				}else{
				    throw new \InvalidArgumentException("Upload image gagal!",0);
				}
			
				return [
					'codestatus'	=> 'S',
					'message'		=> 'Sukses',
					'resultdata'	=> [
						'kode' => 'upload image sukses'
					],
				];
		} catch (\InvalidArgumentException $e) {
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	/**
	* @api {get} /api/v1/mdr/get-image/:nama_file 3. Get View Image
	* @apiSampleRequest off
	* @apiVersion 0.1.0
	* @apiName get_Image
	* @apiGroup Function SAP
	* @apiPermission public
	* @apiDescription digunakan untuk menampilkan gambar yang sudah diupload sebelumnya
	*
	*
	* @apiParamExample {string} Request-Example:
 	* {
 	*	"nama_file": image.jpg
 	* }
	*
	*
	* @apiErrorExample {json} Error-Response:
	* {
	*	"codestatus": "E",
    *	"message": "Error Message",
    *	"resultdata": [],
	* }
	*/
	public function get_Image($image)
	{
		try {			
		
			//$connftp = "ftp://ict:p@ssw0rd@dm1n2017@172.16.100.52/MDR_image/$image";
			$connftp = "ftp://".$this->user_ftp.":".$this->pass_ftp."@".$this->host_ftp."/".$this->folder_ftp."/$image";
			$ftp_image = [
				$connftp
			];
			/*$url_image = [
				"http://172.16.100.11/app_mdr/public/index.php/api/v1/mdr/get-image-url/$image",
				"http://172.16.100.12/app_mdr/public/index.php/api/v1/mdr/get-image-url/$image",
			];*/

				$get = file_get_contents($ftp_image[0]);
				if(!$get){
					throw new \InvalidArgumentException("Gambar tidak ada!", 1);
				}else{
					$file = $get;
				}
			/*die();
			if(empty($image)) throw new \InvalidArgumentException("Parameter image tidak boleh kosong!", 1);
			
			foreach ($this->path_image_all as $key) {
				$destination = $key['config'];
				$file = fopen($destination.$image, 'r');
				if($file){
					break;
				}else{
					continue;
				}
			}
			if(!$file)throw new \InvalidArgumentException("Image tidak ditemukan", 1);
			$file = fread($file,filesize($destination.$image));*/
			header('Content-Type: image/jpeg');
			return $file;
		} catch (\InvalidArgumentException $e) {
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];	
		}
	}

	public function get_ImageUrl($image)
	{
		try {
			if(empty($image)) throw new \InvalidArgumentException("Parameter image tidak boleh kosong!", 1);
			
			foreach ($this->path_image_all as $key) {
				$destination = $key['config'];
				$file = fopen($destination.$image, 'r');
				if($file){
					break;
				}else{
					continue;
				}
			}
			if(!$file)throw new \InvalidArgumentException("Image tidak ditemukan", 1);

			$file = fread($file,filesize($destination.$image));
			header('Content-Type: image/jpeg');
			return $file;
		} catch (\InvalidArgumentException $e) {
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];	
		}
	}

	/**
	* @api {post} /api/v1/mdr/create-mdr 4. Create Transaksi MDR
	* @apiVersion 0.1.0
	* @apiName create_Mdr
	* @apiGroup Function SAP
	* @apiPermission public
	* @apiDescription digunakan untuk create transaksi MDR via aplikasi mobile
	*
	*
	* @apiParam {String} token  Required
	* @apiParam {String} orig_order  Required
	* @apiParam {String} mdr_desc  Required
	* @apiParam {String} op_text  Required
	* @apiParam {String} mdr_subc  Required
	* @apiParam {String} smr_psessi  Required
	* @apiParam {String} smr_sta_type  Required
	* @apiParam {String} smr_sta_from  Required
	* @apiParam {String} smr_sta_to  Required
	* @apiParam {String} smr_str_from  Required
	* @apiParam {String} smr_str_to  Required
	* @apiParam {String} smr_wl_from  Required
	* @apiParam {String} smr_wl_to  Required
	* @apiParam {String} smr_bl_from  Required
	* @apiParam {String} smr_bl_to  Required
	* @apiParam {String} smr_zone  Required
	* @apiParam {String} smr_clk_pst  Required
	* @apiParam {String} smr_defect_limit  Required
	* @apiParam {String} smr_urgrp  Required
	* @apiParam {String} smr_urcod  Required
	* @apiParam {String} smr_urtxt  Required
	*
	*
	* @apiSuccess {String} codestatus Response Status.
	* @apiSuccess {String} message Response Message.
	* @apiSuccess {Array[]} resultdata Response Data.
	* @apiSuccess {String} resultdata.id ID Transaksi.
	*
	* @apiSuccessExample {json} Success-Response:
	* {
	*	"codestatus": "S",
    *	"message": "Sukses",
    *	"resultdata": {
    *		"id": "xx",
    *	}
	* }
	*
	*
	* @apiErrorExample {json} Error-Response:
	* {
	*	"codestatus": "E",
    *	"message": "Error Message",
    *	"resultdata": [],
	* }
	*/
	public function create_Mdr()
	{
		error_reporting(0);
		try {
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_mdr->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$xdata = $_POST;
			
			//pisah Text
			$ltext = $this->pisahText($_POST['mdr_desc']);
			
			//mandatory
			//$no_order = sprintf('%012s',$no_order);
			$no_order = $_POST['orig_order'];
			$orig_order = sprintf('%012s',$no_order);
			$mdr_desc = $_POST['mdr_desc'];
			$op_text = $_POST['op_text'];
			$mdr_grup = trim($_POST['mdr_grup']);
			$mdr_cod = $_POST['mdr_cod'];			
			
			$nopeg = $_POST['ernam'];
			//$nopeg = "533207";
			$floc = $_POST['floc'];
			$m_workcenter = $_POST['mworkcenter'];
			$plant = $_POST['workcenter'];
			$pm_ps = $_POST['pm_ps'];
			$zzmat = $_POST['mat'];		
			$revnr = $_POST['revnr'];
			$revtxt = $_POST['revtxt'];
			
			$maintplan = $_POST['certifauth'];
			
			
			if ($mdr_grup == "ZPM-STR"){
					//optional
					//$smr_psessi = $_POST['smr_psessi'];
					$smr_sta_type = $_POST['smr_sta_type'];
					$smr_sta_from = $_POST['smr_sta_from'];
					$smr_sta_to = $_POST['smr_sta_to'];

					$smr_str_from = $_POST['smr_str_from'];
					$smr_str_to = $_POST['smr_str_to'];

					$smr_wl_from = $_POST['smr_wl_from'];
					$smr_wl_to = $_POST['smr_wl_to'];

					$smr_bl_from = $_POST['smr_bl_from'];
					$smr_bl_to = $_POST['smr_bl_to'];

					$smr_zone = $_POST['smr_zone'];
					$smr_clk_pst = $_POST['smr_clk_pst'];
					$smr_defect_limit = $_POST['smr_defect_limit'];
					
					$smr_objgrp = $_POST['smr_objgrp'];
					$smr_objcod = $_POST['smr_objcod'];
					$smr_urgrp = $_POST['smr_urgrp'];
					$smr_urcod = $_POST['smr_urcod'];
					$smr_urtxt = $_POST['smr_urtxt'];
			}
			
			$SOAP_AUTH = array( 
				'login'    => $this->user_wsdl,
                'password' => $this->pass_wsdl,				
            );
			
			//BAPI
			$WSDL = $this->base_url."assets/PROD_SI_MDR_BAPI_CRTD.wsdl";
			
			//$WSDL = $this->base_url."assets/QA_SI_MDR_BAPI_CRTDService.wsdl";

			$this->soap->SoapClient($WSDL,$SOAP_AUTH);
			$params = array(		 			
					'P_AUFNR' => $orig_order,
					'P_ERNAM' => $nopeg,
					'P_TPLNR' => $floc,
					'P_ADPSP' => $pm_ps,
					'P_QMCOD' => $mdr_cod,
					'P_QMGRP' => $mdr_grup,
					//'P_QMTXT' => $mdr_desc,
					
					'P_USR00' => $maintplan,
					'P_ARBPL' => $m_workcenter,
					'P_WERKS' => $plant,
					'P_LTXA1' => $op_text,
					
					'P_LTEXT' => array(  'FELD1' => $mdr_desc,
										  'FELD2' => '',							
										  'FELD3' => '*',
										  'FELD4' => ''									
									),	
							
				);
				
				if ($mdr_grup == "ZPM-STR"){
					$param2 = array(		
						
						//'P_ZZDEFORD' => $orig_order,	
						'P_STA_TYPE' => $smr_sta_type,							
						'P_ZZSTA_FROM' => $smr_sta_from,
						'P_ZZSTA_TO' => $smr_sta_to,
						'P_ZZSTR_FROM' => $smr_str_from,
						'P_ZZSTR_TO' => $smr_str_to,
						'P_ZZWL_FROM' => $smr_wl_from,
						'P_ZZWL_TO1' => $smr_wl_to,
						'P_ZZBL_FROM' => $smr_bl_from,
						'P_ZZBL_TO1' => $smr_bl_to,				
						'P_ZZMAT' => $zzmat,				
						
						'P_ZZWL_TO' => $smr_zone,
						'P_ZZBL_TO' => $smr_clk_pst,
						'P_OTGRP' => $smr_objgrp,
						'P_OTEIL' => $smr_objcod,				
						'P_URGRP' => $smr_urgrp,
						'P_URCOD' => $smr_urcod,
						'P_CORLIMIT' => $smr_defect_limit,
						'P_URTXT' => $smr_urtxt,
						
					);
					
					$aaa = array_merge($params, $param2);
					$params = $aaa;
				}
								
				//echo "</pre>"; print_r($params); echo "</pre>"; die();
		    
		    $result = $this->soap->SI_MDR_BAPI_CRTD($params);
			
			if($result->CODE_STATUS == "S"){
		    	
				$r['R_REVNR'] = $revnr;
				$r['R_MDR_ORD'] = $result->AUFNR_NEW;
				$r['R_AUFNR_TMP'] = $result->AUFNR_TMP;
				$r['R_QMNUM'] = $result->QMNUM;
				
				$aaa = array_merge($xdata, $r);
				$xdata = $aaa;
				
				$transid = $this->insert_trans('SUKSES',$xdata,$ltext);
		    	return [
					'codestatus'	=> 'S',
					'message'		=> $result->MESSAGE,
					'resultdata'	=> [
						'AUFNR_NEW' => $result->AUFNR_NEW,
						'AUFNR_TMP' => $result->AUFNR_TMP,
						'QMNUM' => $result->QMNUM,
						'TRANS_ID' => $transid["id"]						
					],
				];
				
		    }else{
				$transid = $this->insert_trans('GAGAL',$xdata,$ltext);
		    	//throw new \InvalidArgumentException("Failed, Error Found!", 1);
				throw new \InvalidArgumentException($result->MESSAGE, 1);
		    	
		    }
		} catch (\InvalidArgumentException $e) {
			$transid = $this->insert_trans('GAGAL',$xdata,$ltext);
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];	
		}
	}
	
	public function create_Notif()
	{
		error_reporting(0);
		try {
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_mdr->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$xdata = $_POST;
			
			//pisah Text
			$ltext = $this->pisahText($_POST['mdr_desc']);
			
			
			//mandatory
			//$no_order = sprintf('%012s',$no_order);
			$no_order = $_POST['orig_order'];
			$orig_order = sprintf('%012s',$no_order);
			$mdr_desc = $ltext[0];//$_POST['mdr_desc'];
			$op_text = $_POST['op_text'];
			//$mdr_subc = $_POST['mdr_subc'];
			$mdr_grup = trim($_POST['mdr_grup']);
			$mdr_cod = $_POST['mdr_cod'];			
			
			$nopeg = $_POST['ernam'];
			//$nopeg = "533207";
			$floc = $_POST['floc'];
			$m_workcenter = $_POST['mworkcenter'];
			$workcenter = $_POST['workcenter'];
			$pm_ps = $_POST['pm_ps'];
			$zzmat = $_POST['mat'];		
			$revnr = $_POST['revnr'];
			$revtxt = $_POST['revtxt'];
			
			
			if ($mdr_grup == "ZPM-STR"){
					//optional
					//$smr_psessi = $_POST['smr_psessi'];
					$smr_sta_type = $_POST['smr_sta_type'];
					$smr_sta_from = $_POST['smr_sta_from'];
					$smr_sta_to = $_POST['smr_sta_to'];

					$smr_str_from = $_POST['smr_str_from'];
					$smr_str_to = $_POST['smr_str_to'];

					$smr_wl_from = $_POST['smr_wl_from'];
					$smr_wl_to = $_POST['smr_wl_to'];

					$smr_bl_from = $_POST['smr_bl_from'];
					$smr_bl_to = $_POST['smr_bl_to'];

					$smr_zone = $_POST['smr_zone'];
					$smr_clk_pst = $_POST['smr_clk_pst'];
					$smr_defect_limit = $_POST['smr_defect_limit'];
					
					$smr_objgrp = $_POST['smr_objgrp'];
					$smr_objcod = $_POST['smr_objcod'];
					$smr_urgrp = $_POST['smr_urgrp'];
					$smr_urcod = $_POST['smr_urcod'];
					$smr_urtxt = $_POST['smr_urtxt'];
			}
				
			/* $SOAP_AUTH = array( 
				'login'    => 'G533207',
                'password' => 'Ecco2018',				
            ); */
			
			$SOAP_AUTH = array( 
				'login'    => $this->user_wsdl,
                'password' => $this->pass_wsdl,				
            );
			
			//$WSDL = $this->base_url."assets/qa_cekorder_sap.wsdl";
			//$WSDL = $this->base_url."assets/SI_MDR_NOTIF_CRTDService.wsdl";
			$WSDL = $this->base_url."assets/PROD_SI_MDR_NOTIF_CRTD.wsdl";

			$this->soap->SoapClient($WSDL,$SOAP_AUTH);
			$params = array(		 				
				'P_AUFNR' => $orig_order,
				'P_ERDAT' => date('Ymd'),
				'P_ERNAM' => $nopeg,
				'P_LTXA1' => $op_text,
				'P_QMCOD' => $mdr_cod,
				'P_QMGRP' => $mdr_grup,
				'P_QMTXT' => $mdr_desc,
				'P_TPLNR' => $floc,
				'P_VAPLZ' => $m_workcenter,
				'P_VAWRK' => $workcenter,
				'P_ADPSP' => $pm_ps,
				'LI_TEXTS' => array(
								array(  'TDFORMAT' => '*',
										'TDLINE' => $ltext[0]										
										),
								array(  'TDFORMAT' => '*',
										'TDLINE' => $ltext[1]									
										),
								array( 	'TDFORMAT' => '',
											'TDLINE' => $ltext[2]										
										)
							
							),				
				);
				
				if ($mdr_grup == "ZPM-STR"){
					$param2 = array(	
						'P_STA_TYPE' => $smr_sta_type,		
						'P_ZZDEFORD' => $orig_order,	
						
						'P_ZZSTA_FROM' => $smr_sta_from,
						'P_ZZSTA_TO' => $smr_sta_to,
						'P_ZZSTR_FROM' => $smr_str_from,
						'P_ZZSTR_TO' => $smr_str_to,
						'P_ZZWL_FROM' => $smr_wl_from,
						'P_ZZWL_TO1' => $smr_wl_to,
						'P_ZZBL_FROM' => $smr_bl_from,
						'P_ZZBL_TO1' => $smr_bl_to,				
						'P_ZZMAT' => $zzmat,				
						
						'P_ZZWL_TO' => $smr_zone,
						'P_ZZBL_TO' => $smr_clk_pst,
						'P_OTGRP' => $smr_objgrp,
						'P_OTEIL' => $smr_objcod,				
						'P_URGRP' => $smr_urgrp,
						'P_URCOD' => $smr_urcod,
						'P_CORLIMIT' => $smr_defect_limit,
						'P_URTXT' => $smr_urtxt,
						'P_URL' => "",
					);
					
					$aaa = array_merge($params, $param2);
					$params = $aaa;
				}
				
				
				//echo "</pre>"; print_r($params); echo "</pre>"; die();
		    
		    $result = $this->soap->SI_MDR_NOTIF_CRTD($params);
			
			if($result->AUFNR_NEW != ""){
		    	//$kode_order = "order valid";
				
				
				$r['R_REVNR'] = $revnr;
				$r['R_MDR_ORD'] = $result->AUFNR_NEW;
				$r['R_AUFNR_TMP'] = $result->AUFNR_TMP;
				$r['R_QMNUM'] = $result->QMNUM;
				
				$aaa = array_merge($xdata, $r);
				$xdata = $aaa;
				
				$transid = $this->insert_trans('SUKSES',$xdata,$ltext);
		    	return [
					'codestatus'	=> 'S',
					'message'		=> $result->MESSAGE,
					'resultdata'	=> [
						'AUFNR_NEW' => $result->AUFNR_NEW,
						'AUFNR_TMP' => $result->AUFNR_TMP,
						'QMNUM' => $result->QMNUM,
						'TRANS_ID' => $transid["id"]
						
					],
				];
				
		    }else{
				$transid = $this->insert_trans('GAGAL',$xdata,$ltext);
		    	//throw new \InvalidArgumentException("Failed, Error Found!", 1);
				throw new \InvalidArgumentException($result->MESSAGE, 1);
		    	
		    }
		} catch (\InvalidArgumentException $e) {
			$transid = $this->insert_trans('GAGAL',$xdata,$ltext);
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];	
		}
	}
	
	public function insert_trans($tipe, $prm, $ltext){
		$table = ($tipe == "SUKSES") ?  "TBL_MDR_MOBILE_TRANSACTION" : "TBL_MDR_MOBILE_TRANS_FAILED" ;
									
		try{
			$xparam = [
				"orig_order" => $prm['orig_order'],
				"mdr_desc" => $ltext[0], //$prm['mdr_desc'],
				"op_text" => $prm['op_text'],
				"mdr_grup" => $prm['mdr_grup'],
				"mdr_cod" => $prm['mdr_cod'],
				"tplnr" => $prm['floc'],
				"revnr" => $prm['R_REVNR'],
				"revtx" => $prm['revtxt'],
				"mat" => $prm['mat'],
				"vaplz" => $prm['mworkcenter'],
				"vawrk" => $prm['workcenter'],
				"pm_ps" => $prm['pm_ps'],
				"ernam" => $prm['ernam'],
				
				//hasil Wsdl
				"mdr_order" => $prm['R_MDR_ORD'],
				"no_notif" => $prm['R_QMNUM'],
				
				
				"created_date" => date('Y-m-d H:i:s'),
				"status" => 0,
				"longtxt" => $prm['mdr_desc']
			];
			
			if($prm['mdr_grup'] == "ZPM-STR"){				
				//Optional
				//"smr_psessi" => $prm[],
				$opt_param = [				
						"smr_sta_type" => $prm['smr_sta_type'],
						"smr_sta_from" => $prm['smr_sta_from'],
						"smr_sta_to" => $prm['smr_sta_to'],
						"smr_str_from" => $prm['smr_str_from'],
						"smr_str_to" => $prm['smr_str_to'],
						"smr_wl_from" => $prm['smr_wl_from'],
						"smr_wl_to" => $prm['smr_wl_to'],
						"smr_bl_from" => $prm['smr_bl_from'],
						"smr_bl_to" => $prm['smr_bl_to'],
						"smr_zone" => $prm['smr_zone'],
						"smr_clk_pst" => $prm['smr_clk_pst'],
						"smr_defect_limit" => $prm['smr_defect_limit'],
						"smr_objgrp" => $prm['smr_objgrp'],
						"smr_objcod" => $prm['smr_objcod'],
						"smr_urgrp" => $prm['smr_urgrp'],
						"smr_urcod" => $prm['smr_urcod'],
						"smr_urtxt" => $prm['smr_urtxt'],];
				$aaa = array_merge($xparam, $opt_param);
				$xparam = $aaa;
			}

			//$insert = $this->M_mdr->insert($xparam,'TBL_MDR_MOBILE_TRANSACTION');
			$insert = $this->M_mdr->insert($xparam,$table);
			//if(!$insert) throw new \InvalidArgumentException("Insert transaksi mdr gagal!", 1);
			if(!$insert) { $insert = 0;}

			return  $insert;
		} catch (\InvalidArgumentException $e) {
			return ;	
		}
		
	}
	function pisahText($stre){		
		$rslt[0] = substr($stre,0,40);
		$rslt[1] = substr($stre,40,132);
		$rslt[2] = substr($stre,172,132);	
	return $rslt;
	}
}
?>