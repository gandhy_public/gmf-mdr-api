<?php
namespace MdrApp\Controller;
use MdrApp\Model\M_master;
date_default_timezone_set("Asia/Jakarta");

class Master
{
	protected $con;
	protected $M_master;
	protected $con_crm;

	public function koneksi($conn,$conn_crm)
	{
		$this->con = $conn;
		$this->M_master = new M_master($conn);
		
		$this->con_crm = $conn_crm;
	}

	/**
	* @api {post} /api/v1/master/get-cause_code 1. Get Cause Code
	* @apiVersion 0.1.0
	* @apiName get_CauseCode
	* @apiGroup Master
	* @apiPermission public
	* @apiDescription digunakan untuk mengambil data Cause Code dari database
	*
	*
	* @apiParam {String} token  Required Token for use this API.
	* @apiParam {String[]} [select[]=*]  Optional select parameter.
	* @apiParam {String[]} [where[]=null]  Optional where condition parameter.
	*
	* @apiParamExample {json} Request-Example:
	* in Try Out
	* {
	*	"token": your token
	*	"select": column name, column name
	*	"where": column name = foo, column name like %foo%
	* }
	* in Script PHP
	* <php
	* 	$select = array("column name","column name");
	* 	$where = array("column name='foo'","column name like %foo%");
	* ?>
	*
	*
	* @apiSuccess {String} codestatus Response Status.
	* @apiSuccess {String} message Response Message.
	* @apiSuccess {Array[]} resultdata Response Data.
	* @apiSuccess {String} resultdata.column_name name Data dari kolom yang dipilih.
	*
	* @apiSuccessExample {json} Success-Response:
	*{
	*	"codestatus": "S",
    *	"message": "Sukses",
    *	"resultdata": {
    *		{
    *			"id_cause": "ZPM-STR_AC",
	*			"group_code": "ZPM-STR",
	*			"subcause": "AC",
	*			"desc_causecode": "Accidental damage"
    *		},
    *		{
    *			"id_cause": "ZPM-STR_BL",
	*			"group_code": "ZPM-STR",
	*			"subcause": "BL",
	*			"desc_causecode": "Blockage"
    *		}
    *	}
	*}
	*
	*
	* @apiErrorExample {json} Error-Response:
	* {
	*	"codestatus": "E",
    *	"message": "Error Message",
    *	"resultdata": [],
	* }
	*/
	public function get_CauseCode()
	{
		error_reporting(0);
		try
		{
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_master->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$select = $_POST['select'];
			if(!isset($select) || empty($select)){
				$select = [];
			}
			$where = $_POST['where'];
			if(!isset($where) || empty($where)){
				$where = "";
			}else{
				$where = "WHERE ".implode(" AND ", $where);
			}
			$query = $this->M_master->select_master($select,$where,'TBL_MDR_MOBILE_CAUSE_CODE');

			return $query;
		}
		catch (\InvalidArgumentException $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	/**
	* @api {post} /api/v1/master/get-mdr_code 2. Get MDR Code
	* @apiVersion 0.1.0
	* @apiName get_MdrCode
	* @apiGroup Master
	* @apiPermission public
	* @apiDescription digunakan untuk mengambil data MDR Code dari database
	*
	*
	* @apiParam {String} token  Required Token for use this API.
	* @apiParam {String[]} [select[]=*]  Optional select parameter.
	* @apiParam {String[]} [where[]=null]  Optional where condition parameter.
	*
	* @apiParamExample {json} Request-Example:
	* in Try Out
	* {
	*	"token": your token
	*	"select": column name, column name
	*	"where": column name = foo, column name like %foo%
	* }
	* in Script PHP
	* <php
	* 	$select = array("column name","column name");
	* 	$where = array("column name='foo'","column name like %foo%");
	* ?>
	*
	*
	* @apiSuccess {String} codestatus Response Status.
	* @apiSuccess {String} message Response Message.
	* @apiSuccess {Array[]} resultdata Response Data.
	* @apiSuccess {String} resultdata.column_name name Data dari kolom yang dipilih.
	*
	* @apiSuccessExample {json} Success-Response:
	*{
	*	"codestatus": "S",
    *	"message": "Sukses",
    *	"resultdata": {
    *		{
    *			"code": "ZPM-AVI",
	*			"desc": "Avionic Defect Codes"
    *		},
    *		{
    *			"code": "ZPM-CMC",
	*			"desc": "Complex Maintenance Codes"
    *		}
    *	}
	*}
	*
	*
	* @apiErrorExample {json} Error-Response:
	* {
	*	"codestatus": "E",
    *	"message": "Error Message",
    *	"resultdata": [],
	* }
	*/
	public function get_MdrCode()
	{
		error_reporting(0);
		try
		{
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_master->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$select = $_POST['select'];
			if(!isset($select) || empty($select)){
				$select = [];
			}
			$where = $_POST['where'];
			if(!isset($where) || empty($where)){
				$where = "";
			}else{
				$where = "WHERE ".implode(" AND ", $where);
			}
			$query = $this->M_master->select_master($select,$where,'TBL_MDR_MOBILE_MDRCODE');
			
			return $query;
		}
		catch (\InvalidArgumentException $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	
	/**
	* @api {post} /api/v1/master/get-station_type 3. Get Station Type
	* @apiVersion 0.1.0
	* @apiName get_StationType
	* @apiGroup Master
	* @apiPermission public
	* @apiDescription digunakan untuk mengambil data Station Type dari database
	*
	*
	* @apiParam {String} token  Required Token for use this API.
	* @apiParam {String[]} [select[]=*]  Optional select parameter.
	* @apiParam {String[]} [where[]=null]  Optional where condition parameter.
	*
	* @apiParamExample {json} Request-Example:
	* in Try Out
	* {
	*	"token": your token
	*	"select": column name, column name
	*	"where": column name = foo, column name like %foo%
	* }
	* in Script PHP
	* <php
	* 	$select = array("column name","column name");
	* 	$where = array("column name='foo'","column name like %foo%");
	* ?>
	*
	*
	* @apiSuccess {String} codestatus Response Status.
	* @apiSuccess {String} message Response Message.
	* @apiSuccess {Array[]} resultdata Response Data.
	* @apiSuccess {String} resultdata.column_name name Data dari kolom yang dipilih.
	*
	* @apiSuccessExample {json} Success-Response:
	*{
	*	"codestatus": "S",
    *	"message": "Sukses",
    *	"resultdata": {
    *		{
    *			"tipe_station": "AE",
	*			"desc": "Aileron"
    *		},
    *		{
    *			"code": "BBL",
	*			"desc": "Body Buttock Line"
    *		}
    *	}
	*}
	*
	*
	* @apiErrorExample {json} Error-Response:
	* {
	*	"codestatus": "E",
    *	"message": "Error Message",
    *	"resultdata": [],
	* }
	*/
	public function get_StationType()
	{
		error_reporting(0);
		try
		{
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_master->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$select = $_POST['select'];
			if(!isset($select) || empty($select)){
				$select = [];
			}
			$where = $_POST['where'];
			if(!isset($where) || empty($where)){
				$where = "";
			}else{
				$where = "WHERE ".implode(" AND ", $where);
			}
			$query = $this->M_master->select_master($select,$where,'TBL_MDR_MOBILE_STATION_TYPE');
			
			return $query;
		}
		catch (\InvalidArgumentException $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	/**
	* @api {post} /api/v1/master/get-sub_code 4. Get Sub Code
	* @apiVersion 0.1.0
	* @apiName get_SubCode
	* @apiGroup Master
	* @apiPermission public
	* @apiDescription digunakan untuk mengambil data Sub Code dari database
	*
	*
	* @apiParam {String} token  Required Token for use this API.
	* @apiParam {String[]} [select[]=*]  Optional select parameter.
	* @apiParam {String[]} [where[]=null]  Optional where condition parameter.
	*
	* @apiParamExample {json} Request-Example:
	* in Try Out
	* {
	*	"token": your token
	*	"select": column name, column name
	*	"where": column name = foo, column name like %foo%
	* }
	* in Script PHP
	* <php
	* 	$select = array("column name","column name");
	* 	$where = array("column name='foo'","column name like %foo%");
	* ?>
	*
	*
	* @apiSuccess {String} codestatus Response Status.
	* @apiSuccess {String} message Response Message.
	* @apiSuccess {Array[]} resultdata Response Data.
	* @apiSuccess {String} resultdata.column_name name Data dari kolom yang dipilih.
	*
	* @apiSuccessExample {json} Success-Response:
	*{
	*	"codestatus": "S",
    *	"message": "Sukses",
    *	"resultdata": {
    *		{
    *			"id_subcode": "ZPM-AVI_AC",
	*			"group_code": "ZPM-AVI",
	*			"subcode": "AC",
	*			"desc_subcode" "Arcing-connector"
    *		},
    *		{
    *			"id_subcode": "ZPM-AVI_AS",
	*			"group_code": "ZPM-AVI",
	*			"subcode": "AS",
	*			"desc_subcode" "Arcing-splice"
    *		}
    *	}
	*}
	*
	*
	* @apiErrorExample {json} Error-Response:
	* {
	*	"codestatus": "E",
    *	"message": "Error Message",
    *	"resultdata": [],
	* }
	*/
	public function get_SubCode()
	{
		error_reporting(0);
		try
		{
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_master->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$select = $_POST['select'];
			if(!isset($select) || empty($select)){
				$select = [];
			}
			$where = $_POST['where'];
			if(!isset($where) || empty($where)){
				$where = "";
			}else{
				$where = "WHERE ".implode(" AND ", $where);
			}
			$query = $this->M_master->select_master($select,$where,'TBL_MDR_MOBILE_SUBCODE');
			
			return $query;
		}
		catch (\InvalidArgumentException $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}	
	
	public function get_Plant()
	{
		error_reporting(0);
		try
		{
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_master->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$select = $_POST['select'];
			if(!isset($select) || empty($select)){
				$select = [];
			}
						
			$search = $_POST['search'];
			if(!isset($search) || empty($search)){
				$where = "";
			}else{
				$where = "WHERE name like '%$search%' OR city like '%$search%'";
			}
			$query = $this->M_master->select_master($select,$where,'TBL_MDR_MOBILE_PLANT');
			
			return $query;
		}
		catch (\InvalidArgumentException $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}	
	public function get_Workcenter()
	{
		error_reporting(0);
		try
		{
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$plant = $_POST['plant'];
			if(!isset($plant)) throw new \InvalidArgumentException("Plant not set", 1);
			if(empty($plant)) throw new \InvalidArgumentException("Plant not set", 1);
			
			$cek_token = $this->M_master->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$select = $_POST['select'];
			if(!isset($select) || empty($select)){
				$select = [];
			}
			
			$where = "WHERE arbpl is not null ";
			
			if(!isset($plant) || empty($plant)){
				$where .= "";
			}else{
				$where .= "and werks = '$plant' ";
			}
			
			$search = $_POST['search'];
			if(!isset($search) || empty($search)){
				$where .= "";
			}else{
				$where .= "and name like '%$search%'";
			}
			$query = $this->M_master->select_master($select,$where,'TBL_MDR_MOBILE_WCENTER');
			
			return $query;
		}
		catch (\InvalidArgumentException $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}	
	
	public function get_Aircraft()
	{
		error_reporting(0);
		try
		{
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_master->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);
			
			$p_revnr  = $_POST['revnr'];
			 
			$where = "";
			//$prefix = $this->con_crm['dbname'].".".$this->con_crm['schema'];
			
			$tbl_revision = "M_REVISION";
			//$f_revnr = "M_REVISION.REVNR";
			//$f_tplnr = "M_REVISION.tplnr";
			$f_revnr = "REVNR";
			$f_tplnr = "tplnr";
			
					
			//$sql = "select tplnr from $tbl_mdr where status = 0 $where ";
			$sql = "select top 1 $f_tplnr from $tbl_revision where CAST($f_revnr AS int) = CAST('$p_revnr' AS int)";

			
			//echo $sql;die();
			$stmt = $this->con_crm->prepare($sql);
			$result = $stmt->execute();
			$result = $stmt->fetchAll();
			
			$rsltdata = array();
			foreach ($result as $key => $val){
				$rsltdata['tplnr'] = trim($val['tplnr']);
			}
			
			$respon["codestatus"] = 'S';
			$respon["message"] = "Sukses";
			$respon["resultdata"] = $rsltdata;
			
			return $respon;
		}
		catch (\InvalidArgumentException $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}
		
	public function get_MdrList()
	{
		error_reporting(0);
		try
		{
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_master->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);
			
			// revnr,  pmorder : revnr , tbl_transaction : revnr
			 /* airreg, 	pmorder : tplnr , tbl_transaction : tplnr
			 datefr,   pmorder : erdat , tbl_transaction : createddate
			 dateto,	pmorder : erdat , tbl_transaction : createddate
			 origin,  pmorder : '/SMR/DEFORD' , tbl_transaction : orig order
			 defect 	pmorder : 'maktx' , tbl_transaction : mdr_desc
			 */
			 
			//Parameter filter
			 $revnr  = $_POST['revnr'];
			 $airreg = $_POST['airreg'];
			 $datefr = $_POST['datefr'];
			 $dateto = $_POST['dateto'];
			 $origin = $_POST['origin'];
			 $defect = $_POST['defect'];
			 
			 $wherecrm = "";
			 $wheretgl = "";
			 $wheremobile = "";
			 
			 if ($revnr != ""){
				 $wheremobile .= " and CAST(revnr AS int) = CAST('$revnr' AS int) ";
				 $wherecrm .= " and CAST(t.REVNR AS int) = CAST('$revnr' AS int) ";
			 }
			 /* if ($airreg != ""){
				 $wheremobile .= " and tplnr = '$airreg' ";
				 $wherecrm .= " and LTRIM(RTRIM(l.TPLNR)) = '$airreg' ";
			 } */
			 if ($datefr != ""){
				 $fr = explode("/",$datefr);
				 $dtfr = $fr[2]."-".$fr[1]."-".$fr[0];
				 
				 if ($dateto != ""){
					 $to = explode("/",$dateto);
					 $dto = $to[2]."-".$to[1]."-".$to[0];
					 $wheremobile .= " and CAST(created_date as date) between '$dtfr' and '$dto' ";
					 $wheretgl .= "where xtgl BETWEEN '$dtfr' and '$dto'";
				 }else{
					 $wheremobile .= " and CAST(created_date as date) = '$dtfr' ";
					 $wheretgl .= "where xtgl = '$dtfr' ";
				 }
				 
			 }
			 if ($origin != ""){
				$wheremobile .= " and orig_order = '$origin' ";
				//$wherecrm .= " and h.[/SMR/DEFORD] like '%$origin' ";
				$wherecrm .= " and t.JC_REFF like '%$origin' ";
			 }
			 if ($defect != ""){
				//$wheremobile .= " and mdr_desc like '%$defect%' ";
				$wheremobile .= " and longtxt like '%$defect%' ";
				$wherecrm .= " and t.KTEXT like '%$defect%' ";
			 }
			 
						
			$tbl_mdr = "TBL_MDR_MOBILE_TRANSACTION";
			
			/* $sqlmdr = "select *,CONVERT(varchar,created_date , 105) as tglcreate
					from $tbl_mdr where status = 0 $wheremobile ";	 */
					
					
			$sqlmdr = "select a.*,CONVERT(varchar,a.created_date , 105) as tglcreate,b.img
						from TBL_MDR_MOBILE_TRANSACTION a
						left join (select mdr_order,count(id) as img from TBL_MDR_MOBILE_IMAGE GROUP BY mdr_order) b 
						on a.mdr_order = b.mdr_order 
						where a.status = 0 $wheremobile ";
					
			//echo $sqlmdr;die();
			$stmt = $this->con->prepare($sqlmdr);
			$rslt = $stmt->execute();
			$rslt = $stmt->fetchAll();
			//print_r($rslt);
			
			$dtamdr = array();
			foreach($rslt as $k => $row){
				$key = trim($row['mdr_order']);
				//echo "$key";
				$v['revnr'] = (($row['revnr'] == "") ?  $row['revnr'] : (String)(int)$row['revnr']);//$row['revnr'];
				$v['mdrorder'] = (($row['mdr_order'] == "") ?  $row['mdr_order'] : (String)(int)$row['mdr_order']);// $row['mdr_order'];
				$v['origorder'] =  (($row['orig_order'] == "") ?  $row['orig_order'] : (String)(int)$row['orig_order']);//$row['orig_order'];
				
				$v['tplnr'] = $row['tplnr'];
				
				$v['revtx'] = $row['revtx'];				
				$v['mdrdesc'] = $row['mdr_desc'];
				$v['longtxt'] = $row['longtxt'];
				
				$v['mdrdate'] = $row['tglcreate'];
				$v['ernam'] = $row['ernam'];
				$v['mdrstatus'] = 'OPEN';
				$v['sapstatus'] = 'OPEN';	

				//flag icon image
				if($row['img'] ==""){
					$v['attachimg'] = "0";
				}else{
					$v['attachimg'] = $row['img'];
				}
				
				$dtamdr[$key] = $v;
			}
			
			//$prefix = $this->con_crm['dbname'].".".$this->con_crm['schema'];
						//print_r($dtamdr);die();
			//define
			$tbl_mordertext = "M_ORDERTEXT";
			$tbl_pmorder = "M_PMORDER";
			$tbl_pmorderh = "M_PMORDERH";
			$tbl_revision = "M_REVISION";
			$tbl_tb_pmorder = "TB_M_PMORDER";
			
			/* $sql = "with tmp as  (
					select  convert(date,(SUBSTRING(ERDAT, 5, 4)+'/'+
					SUBSTRING(ERDAT, 3, 2)+'/'+
					SUBSTRING(ERDAT, 1, 2)
					)) as xtgl, *
					from M_PMORDER
					where aufnr = '801441863'
					)
					select * from tmp where 
					xtgl BETWEEN '2017-01-02' and '2017-01-08'"; */
			
			/*$sqlcrm = "select * ,CONVERT(varchar,xtgl, 105) as tglcreate from 
						(
							select DiSTiNCT h.aufnr, h.ernam, h.erdat, h.[/SMR/DEFORD] as mdr_order, 
							l.ktext, l.tplnr , r.revnr, r.revtx, t.mdr_status as ordstatus,
							convert(date,(SUBSTRING(h.ERDAT, 5, 4)+'/'+
									SUBSTRING(h.ERDAT, 3, 2)+'/'+
									SUBSTRING(h.ERDAT, 1, 2)
									)) as xtgl
							from $tbl_pmorderh h
							LEFT JOIN $tbl_pmorder l on h.AUFNR = l.AUFNR
							LEFT JOIN $tbl_tb_pmorder t on h.AUFNR = t.AUFNR
							LEFT JOIN $tbl_revision r on CAST(r.REVNR AS BIGINT) = CAST(h.REVNR AS int)
							where LTRIM(RTRIM(h.AUART)) = LTRIM(RTRIM('GA02'))
								  $wherecrm 
						) a 
						$wheretgl
						"; */
				
			$sqlcrm = "select * ,CONVERT(varchar,xtgl, 105) as tglcreate from 
						(
							select DiSTiNCT t.aufnr, t.ernam, t.erdat, t.JC_REFF as orig_order, 
							t.ktext, r.tplnr , t.revnr, r.revtx, t.mdr_status as ordstatus,
							CONVERT(date,t.erdat, 105) as xtgl,
							(ot.LONGTEXT + ot.LONGTEXT1 +ot.LONGTEXT2 +ot.LONGTEXT3 + ot.LONGTEXT4 +
							ot.LONGTEXT5 + ot.LONGTEXT6) as longtxt							
							FROM $tbl_tb_pmorder t
							LEFT join $tbl_mordertext ot on ot.AUFNR = t.AUFNR
							LEFT JOIN $tbl_revision r on CAST(r.REVNR AS BIGINT) = CAST(t.REVNR AS int)
							where LTRIM(RTRIM(t.AUART)) = LTRIM(RTRIM('GA02'))
									 $wherecrm 
						) a  $wheretgl
						";
			
			//echo $sqlcrm;die();
			$cstmt = $this->con_crm->prepare($sqlcrm);			 
			  $cresult = $cstmt->execute();
			  $cresult = $cstmt->fetchAll();
			
			
			/*
			*	Status SAP
			*	CRTD = Create /Open (1)
			*   REL	= Release / Progress (2)
			*	CLSD = Closed (3)
			
				MANUAL ::
				OPEN 	(1)				
				PROGRESS (2)
				PENDING (3)
				CLOSE 	(4)
			*
			*/
			
			$dtatemp = $dtamdr;
			foreach($cresult as $k => $row){
				$key = trim($row['aufnr']);				
				$v['revnr'] = (($row['revnr'] == "") ?  $row['revnr'] : (String)(int)$row['revnr']);
				
				$v['mdrorder'] = (($row['aufnr'] == "") ?  $row['aufnr'] : (String)(int)$row['aufnr']);//$row['mdr_order'];
				$v['origorder'] = (($row['orig_order'] == "") ?  $row['orig_order'] : (String)(int)$row['orig_order']);//$row['orig_order'];
				
				$v['revtx'] = $row['revtx'];
				$v['tplnr'] =  $row['tplnr'];
				$v['mdrdesc'] = $row['ktext'];
				$v['longtxt'] = trim($row['longtxt']);
				$v['mdrdate'] = $row['tglcreate'];
				$v['ernam'] = $row['ernam'];
				
				if ($row['ordstatus'] == "OPEN" || $row['ordstatus'] == "" || empty($row['ordstatus'])){					
					$v['mdrstatus'] = 'OPEN';
					$v['sapstatus'] = 'OPEN';	
				}else if($row['ordstatus'] == "CLOSED"){					
					$v['mdrstatus'] = 'CLOSE';
					$v['sapstatus'] = 'CLOSE';	
				}else{				
					$v['mdrstatus'] = $row['ordstatus'];//PROGRESS , PENDING
					$v['sapstatus'] = $row['ordstatus'];
				} 
				
				if ($dtatemp[$key]['attachimg'] == ""){
					$v['attachimg'] = "0";
				}else{
					$v['attachimg'] = $dtatemp[$key]['attachimg'];
				}
				
				
				$dtamdr[$key] = $v;
			}
			
			rsort($dtamdr);
			
			
			$rsltdata = array();
			foreach ($dtamdr as $order =>$rs){
				$rsltdata[] = $rs;
			}
			
			//result ex :
			/*  "revnr": "00035122",
				"revtx": "PK-CLU B737-500 SRIWIJAYA C04-CHK DEC 16",
				"tplnr": "PK-CLU",
				"mdr_order": "000801495539",
				"mdr_desc": "TAXI LIGHTS NOT ILLUMINATE",
				"orig_order": null,
				"created_date": "01-02-2017",
				"ernam": "S165010" */
			
			$respon["codestatus"] = 'S';
			$respon["message"] = "Sukses";
			$respon["resultdata"] = $rsltdata;
			
			return $respon;
		}
		catch (\InvalidArgumentException $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}
	public function get_MdrImage()
	{
		error_reporting(0);
		try
		{
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_master->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);
						
			 $mdrno  = $_POST['mdrno'];
			 
			 $where = "";
			 if ($mdrno != ""){
				 $where .= " and mdr_order like '$mdrno' ";
			 }
			 
			$tbl_mdr = "TBL_MDR_MOBILE_IMAGE";
			$sql = "select * from $tbl_mdr where is_delete = 0 $where ";
			
			//echo $sql;die();
			$stmt = $this->con->prepare($sql);
			$result = $stmt->execute();
			$result = $stmt->fetchAll();
			
			foreach( $result as $dat ) {
				 $img[] = $dat['name'];
			}
			$resdata['image'] = $img;

			
			$respon["codestatus"] = 'S';
			$respon["message"] = "Sukses";
			$respon["resultdata"] = $resdata;
			
			return $respon;
		}
		catch (\InvalidArgumentException $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}
}
?>