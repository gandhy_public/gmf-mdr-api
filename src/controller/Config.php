<?php
namespace MdrApp\Controller;
use MdrApp\Model\M_config;
date_default_timezone_set("Asia/Jakarta");

class Config
{
	protected $con;
	protected $M_config;
	protected $base_url;

	public function koneksi($conn)
	{
		$this->con = $conn;
		$this->M_config = new M_config($conn);
		$url = sprintf("%s://%s%s",
			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME'],
			$_SERVER['REQUEST_URI']
		);
		$url = explode("public/index.php", $url);
		$this->base_url = $url[0];
	}

	/**
	* @api {post} /api/v1/config/set-config_path 1. Setting Config Path
	* @apiVersion 0.1.0
	* @apiName set_ConfigPath
	* @apiGroup Configuration
	* @apiPermission public
	* @apiDescription digunakan untuk setting path folder upload data
	*
	*
	* @apiParam {String} token  Required Token for use this API.
	* @apiParam {String} path  Required Path folder.
	* @apiParam {String} type  Required Type Path folder.
	*
	* @apiParamExample {json} Request-Example:
	* in Try Out
	* {
	*	"token": your token
	*	"path": your path folder
	*	"type": your type path (image/APK/.est)
	* }
	*
	*
	* @apiSuccess {String} codestatus Response Status.
	* @apiSuccess {String} message Response Message.
	* @apiSuccess {Array[]} resultdata Response Data.
	*
	* @apiSuccessExample {json} Success-Response:
	*{
	*	"codestatus": "S",
    *	"message": "Sukses",
    *	"resultdata": [],
	*}
	*
	*
	* @apiErrorExample {json} Error-Response:
	* {
	*	"codestatus": "E",
    *	"message": "Error Message",
    *	"resultdata": [],
	* }
	*/
	public function set_ConfigPath()
	{
		try {
			$token = $_POST['token'];
			if(!isset($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			if(empty($token)) throw new \InvalidArgumentException("Token tidak boleh kosong!", 1);
			
			$cek_token = $this->M_config->select(['is_valid'],['token' => $token],'TBL_MDR_MOBILE_LOGIN_LOG');
			if(count($cek_token['resultdata']) == 0) throw new \InvalidArgumentException("Token tidak valid!", 1);
			if(!$cek_token['resultdata'][0]['is_valid']) throw new \InvalidArgumentException("Token tidak valid!", 1);

			$path = $_POST['path'];
			if(!isset($path)) throw new \InvalidArgumentException("Path folder tidak boleh kosong", 1);
			if(empty($path)) throw new \InvalidArgumentException("Path folder tidak boleh kosong",1);

			$type = $_POST['type'];
			if(!isset($type)) throw new \InvalidArgumentException("Type path tidak boleh kosong", 1);
			if(empty($type)) throw new \InvalidArgumentException("Type path tidak boleh kosong",1);

			$param = [
				'config' => $path,
				'type' => $type,
				'create_at' => date('Y-m-d H:i:s'),
				'is_active' => 1
			];

			$param_update = [
				'update_at' => date('Y-m-d H:i:s'),
				'is_active' => 0
			];
			$where_update = [
				'type' => $type,
				'is_active' => 1
			];

			$update = $this->M_config->update($param_update,$where_update,'TBL_MDR_MOBILE_CONFIG');
			if(!$update) throw new \InvalidArgumentException("Update config path gagal!", 1);

			$insert = $this->M_config->insert($param,'TBL_MDR_MOBILE_CONFIG');
			if(!$insert) throw new \InvalidArgumentException("Insert config path gagal!", 1);
		    
		    return [
				'codestatus'	=> 'S',
				'message'		=> 'Sukses',
				'resultdata'	=> [],
			];
		} catch (\InvalidArgumentException $e) {			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

}
?>