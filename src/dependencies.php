<?php
// DIC configuration

$container = $app->getContainer();

$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        $data = [
            'codestatus'    => 'E',
            'message'       => $exception->getMessage(),
            'resultdata'    => [],
        ];
        return $c['response']->withJson($data,200);
    };
};

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

//controller
$container['api'] = function ($c){
	include 'controller/Api.php';
	return new \MdrApp\Controller\Api();
};
$container['auth'] = function ($c){
	include 'controller/Auth.php';
	return new \MdrApp\Controller\Auth();
};
$container['master'] = function ($c){
    include 'controller/Master.php';
    return new \MdrApp\Controller\Master();
};
$container['mdr'] = function ($c){
    include 'controller/Mdr.php';
    return new \MdrApp\Controller\Mdr();
};
$container['config'] = function ($c){
    include 'controller/Config.php';
    return new \MdrApp\Controller\Config();
};

//model
$container['data'] = function ($c){
	//include 'model/Data.php';
	return new \MdrApp\Model\Data();
};

//database
$container['db'] = function ($c) {
    $settings = $c->get('settings')['db'];
	$pdo = new PDO("sqlsrv:server=" . $settings['host'] . ";Database=" . $settings['dbname'],$settings['user'], $settings['pass']);
	
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};


$container['db_crm'] = function ($c) {
    $settings = $c->get('settings')['db_crm'];
	$cpdo = new PDO("sqlsrv:server=" . $settings['host'] . ";Database=" . $settings['dbname'],$settings['user'], $settings['pass']);
	
    $cpdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $cpdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $cpdo;	    
};

//soap
$container['soap'] = function ($c) {
    $base_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
	$settings = $c->get('settings')['user_wsdl'];
	 
	$SOAP_AUTH = array( 
        'login'    => $settings['user'],
        'password' => $settings['pass'],
    );
    //$WSDL = str_replace("public/index.php", "assets/qa_cekorder_sap.wsdl", $base_url);
	//$WSDL = str_replace("public/index.php", "assets/SI_MDR_NOTIF_CRTDService.wsdl", $base_url);
	$WSDL = str_replace("public/index.php", "assets/PROD_SI_MDR_ORDCEK.wsdl", $base_url);
	
	//$WSDL = "http://sapgmfppi.gmf-aeroasia.co.id:50100/dir/wsdl?p=sa/60eecccf376436aeacfaf8a4ab78da57";
    $client = new SoapClient($WSDL,$SOAP_AUTH);
    return $client;
};

//database
$container['wsdl'] = function ($c) {
    $settings = $c->get('settings')['user_wsdl'];	
    return $settings;
};

$container['urlprefix_image'] = function ($c) {
    $settings = $c->get('settings')['uri_img'];	
    return $settings;
};

$container['user_ftp'] = function ($c) {
    $settings = $c->get('settings')['ftp'];	
    return $settings;
};